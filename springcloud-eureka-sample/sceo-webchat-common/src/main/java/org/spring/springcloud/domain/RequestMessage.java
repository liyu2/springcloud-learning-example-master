package org.spring.springcloud.domain;

/**
 * Created by liyu on 2017/11/6.
 */
public class RequestMessage {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
