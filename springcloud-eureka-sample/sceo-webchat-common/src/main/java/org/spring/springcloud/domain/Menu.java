package org.spring.springcloud.domain;

import lombok.Data;

import java.util.List;

/**
 * Created by sang on 2017/12/28.
 */
public class Menu {
    private Long id;
    private String url;
    private String path;
    private Object component;
    private String name;
    private String iconCls;
    private Long parentId;
    private  int isenale; //
    private String addTime; //
    private String addUser ; //
    private String modTime; //
    private String modUser ; //

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getComponent() {
        return component;
    }

    public void setComponent(Object component) {
        this.component = component;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public int getIsenale() {
        return isenale;
    }

    public void setIsenale(int isenale) {
        this.isenale = isenale;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getAddUser() {
        return addUser;
    }

    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }

    public String getModTime() {
        return modTime;
    }

    public void setModTime(String modTime) {
        this.modTime = modTime;
    }

    public String getModUser() {
        return modUser;
    }

    public void setModUser(String modUser) {
        this.modUser = modUser;
    }
    //    private Long menuId;
//    private String menuName;
//    private String menuPath;
//    private String nodePath;
//    private int menuType;
//    private String menuTarget;
//    private Long parentId;
//    private int menuLevel;   //
//    private int functionId ;//
//    private String functionName;//
//    private  int isenale; //
//    private String addTime; //
//    private String addUser ; //
//    private String modTime; //
//    private String modUser ; //
//
//    public Long getMenuId() {
//        return menuId;
//    }
//
//    public void setMenuId(Long menuId) {
//        this.menuId = menuId;
//    }
//
//    public String getMenuName() {
//        return menuName;
//    }
//
//    public void setMenuName(String menuName) {
//        this.menuName = menuName;
//    }
//
//    public String getMenuPath() {
//        return menuPath;
//    }
//
//    public void setMenuPath(String menuPath) {
//        this.menuPath = menuPath;
//    }
//
//    public String getNodePath() {
//        return nodePath;
//    }
//
//    public void setNodePath(String nodePath) {
//        this.nodePath = nodePath;
//    }
//
//    public int getMenuType() {
//        return menuType;
//    }
//
//    public void setMenuType(int menuType) {
//        this.menuType = menuType;
//    }
//
//    public String getMenuTarget() {
//        return menuTarget;
//    }
//
//    public void setMenuTarget(String menuTarget) {
//        this.menuTarget = menuTarget;
//    }
//
//    public Long getParentId() {
//        return parentId;
//    }
//
//    public void setParentId(Long parentId) {
//        this.parentId = parentId;
//    }
//
//    public int getMenuLevel() {
//        return menuLevel;
//    }
//
//    public void setMenuLevel(int menuLevel) {
//        this.menuLevel = menuLevel;
//    }
//
//    public int getFunctionId() {
//        return functionId;
//    }
//
//    public void setFunctionId(int functionId) {
//        this.functionId = functionId;
//    }
//
//    public String getFunctionName() {
//        return functionName;
//    }
//
//    public void setFunctionName(String functionName) {
//        this.functionName = functionName;
//    }
//
//    public int getIsenale() {
//        return isenale;
//    }
//
//    public void setIsenale(int isenale) {
//        this.isenale = isenale;
//    }
//
//    public String getAddTime() {
//        return addTime;
//    }
//
//    public void setAddTime(String addTime) {
//        this.addTime = addTime;
//    }
//
//    public String getAddUser() {
//        return addUser;
//    }
//
//    public void setAddUser(String addUser) {
//        this.addUser = addUser;
//    }
//
//    public String getModTime() {
//        return modTime;
//    }
//
//    public void setModTime(String modTime) {
//        this.modTime = modTime;
//    }
//
//    public String getModUser() {
//        return modUser;
//    }
//
//    public void setModUser(String modUser) {
//        this.modUser = modUser;
//    }
}
