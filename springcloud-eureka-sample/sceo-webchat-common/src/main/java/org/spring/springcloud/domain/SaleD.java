package org.spring.springcloud.domain;

public class SaleD {
    String saleDId;   //记录Id
    String saleMId;  //主表id
    private String productId; //产品id
    private double qtyBuy; //购买数量
    private  double price; //出售价格
    private double amtPay; //应付金额
    private double amtPayed; //已付金额
    private double amtNoPay; //未付金额
    private String saleDate;//销售日期
    private String remark; //说明
    private String addTime; //添加时间
    private String addUser ; //添加人
    private String modTime; //修改时间
    private String modUser ; //修改人
    private int deleteFlag ; //删除标识 0：未删除

    private SaleM saleM;
    private  Product product;

    public String getSaleDId() {
        return saleDId;
    }

    public void setSaleDId(String saleDId) {
        this.saleDId = saleDId;
    }

    public String getSaleMId() {
        return saleMId;
    }

    public void setSaleMId(String saleMId) {
        this.saleMId = saleMId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public double getQtyBuy() {
        return qtyBuy;
    }

    public void setQtyBuy(double qtyBuy) {
        this.qtyBuy = qtyBuy;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAmtPay() {
        return amtPay;
    }

    public void setAmtPay(double amtPay) {
        this.amtPay = amtPay;
    }

    public double getAmtPayed() {
        return amtPayed;
    }

    public void setAmtPayed(double amtPayed) {
        this.amtPayed = amtPayed;
    }

    public double getAmtNoPay() {
        return amtNoPay;
    }

    public void setAmtNoPay(double amtNoPay) {
        this.amtNoPay = amtNoPay;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getAddUser() {
        return addUser;
    }

    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }

    public String getModTime() {
        return modTime;
    }

    public void setModTime(String modTime) {
        this.modTime = modTime;
    }

    public String getModUser() {
        return modUser;
    }

    public void setModUser(String modUser) {
        this.modUser = modUser;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public SaleM getSaleM() {
        return saleM;
    }

    public void setSaleM(SaleM saleM) {
        this.saleM = saleM;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
