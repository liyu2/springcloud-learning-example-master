package org.spring.springcloud.domain;

/**
 * Created by liyu on 2017/09/28.
 */
public class UserInfo {
    private String id;
    private String loginName;//登录名
    private String worldPost; //国际区号
    private String mobileNumber;//手机号码
    private String nationality;//国家
    private String email;//邮箱
    private String password;//密码
    private String name;//备注姓名
    private String state;//状态
    private String certificateType;//证件类型
    private String certificateNumber;//证件号码
    private int nameRealFlag;//实名认证标记
    private int questionFlag;//删除标记

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getWorldPost() {
        return worldPost;
    }

    public void setWorldPost(String worldPost) {
        this.worldPost = worldPost;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public int getNameRealFlag() {
        return nameRealFlag;
    }

    public void setNameRealFlag(int nameRealFlag) {
        this.nameRealFlag = nameRealFlag;
    }

    public int getQuestionFlag() {
        return questionFlag;
    }

    public void setQuestionFlag(int questionFlag) {
        this.questionFlag = questionFlag;
    }

    @Override
    public String toString() {
        return "{" +
                "id:'" + id + '\'' +
                ", loginName:'" + loginName + '\'' +
                ", worldPost:'" + worldPost + '\'' +
                ", mobileNumber:'" + mobileNumber + '\'' +
                ", nationality:'" + nationality + '\'' +
                ", email:'" + email + '\'' +
                ", password:'" + password + '\'' +
                ", name:'" + name + '\'' +
                ", state:'" + state + '\'' +
                ", certificateType:'" + certificateType + '\'' +
                ", certificateNumber:'" + certificateNumber + '\'' +
                ", nameRealFlag:" + nameRealFlag +
                ", questionFlag:" + questionFlag +
                '}';
    }
}
