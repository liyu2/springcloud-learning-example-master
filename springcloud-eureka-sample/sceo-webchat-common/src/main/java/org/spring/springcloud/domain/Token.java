package org.spring.springcloud.domain;

public class Token  {

	private String tokenId;
	private String userId;
	private String token;
	private String startTime;
	private String endTime;

	public Token() {
	}

	public Token(String userId, String token, String startTime, String endTime) {
		this.userId = userId;
		this.token = token;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}