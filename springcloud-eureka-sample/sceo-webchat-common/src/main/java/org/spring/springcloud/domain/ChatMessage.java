package org.spring.springcloud.domain;

public class ChatMessage {
    String FromuserId;
    String toUserId;
    String Message;

    public String getFromuserId() {
        return FromuserId;
    }

    public void setFromuserId(String fromuserId) {
        FromuserId = fromuserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
