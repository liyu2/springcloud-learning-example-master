package org.spring.springcloud.service;

import org.spring.springcloud.domain.City;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by fangzhipeng on 2017/4/6.
 */
@FeignClient(value = "provider-service")
public interface SchedualServiceHi {
    @RequestMapping(value = "/getcity",method = RequestMethod.GET)
    City findOneCity(@RequestParam(value = "cityname") String cityname);


    @RequestMapping(value = "/testFeign",method = RequestMethod.GET)
    String testFeign(@RequestParam(value = "name") String name);

}
