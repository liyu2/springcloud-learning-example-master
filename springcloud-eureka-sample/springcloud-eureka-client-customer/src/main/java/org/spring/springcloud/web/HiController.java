package org.spring.springcloud.web;


import org.spring.springcloud.service.SchedualServiceHi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by fangzhipeng on 2017/4/6.
 */

@RestController
public class HiController {

    @Autowired
    SchedualServiceHi schedualServiceHi;

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public String testFeign(@RequestParam(value = "name", required = true) String name){
        schedualServiceHi.testFeign(name);
        return schedualServiceHi.testFeign(name);
    }

//    @RequestMapping(value = "/getcity",method = RequestMethod.GET)
//    public City getCity(@RequestParam(value = "cityname", required = true) String cityname){
//        schedualServiceHi.findOneCity(cityname);
//        return  schedualServiceHi.findOneCity(cityname);
//    }

}
