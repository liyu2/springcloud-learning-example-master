package org.spring.springcloud.dao;

import org.spring.springcloud.domain.SaleM;

import java.util.List;

/**
 * Created by liyu2 on 2017/10/9.
 */
public interface SaleMDao {
    SaleM selectSaleMById(String mid);

    List<SaleM> selectMAll();

    int addSaleM(SaleM salem);

    int updateSaleM(SaleM salem);
}
