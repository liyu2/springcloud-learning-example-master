package org.spring.springcloud.service;

import org.spring.springcloud.domain.Menu;
import org.spring.springcloud.domain.Role;

import java.util.List;

/**
 * Created by liyu on 2018/5/14.
 */
public interface MenuService {

    public List<Role> getAllRoleSByUrl(String url);

    public List<Menu> getMenusByHrId();

    public List<Menu> menuTree();

    public List<Long> getMenusByRid(Long rid);
}
