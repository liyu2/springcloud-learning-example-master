package org.spring.springcloud.dao;

import org.spring.springcloud.domain.Custom;

import java.util.List;

public interface CustomDao {
    int addCustom(Custom custom);

    int updateCustom(Custom custom);

    Custom selectCustomById(String id);

    List<Custom>  selectAll();
}
