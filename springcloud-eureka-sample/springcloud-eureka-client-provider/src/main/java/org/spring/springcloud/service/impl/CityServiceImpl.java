package org.spring.springcloud.service.impl;


import org.spring.springcloud.dao.CityDao;
import org.spring.springcloud.domain.City;
import org.spring.springcloud.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDao cityDao;

    //存对象，被存对象必须序列化
    @Autowired
    RedisTemplate redisTemplate;

    //存String
    @Autowired
    StringRedisTemplate template;

    //存指定对象，city对象必须序列化
    @Resource(name = "redisTemplate")
    private RedisTemplate<String,City> cityRedisTemplate;

    public City findCityByName(String cityName) {
    //  City city=cityDao.findByName(cityName);
        //设置字符串
        //template.opsForValue().set("mykey100", "helloredis");
        template.opsForValue().set("hk2","hello hk2");
        System.out.println("-----------"+template.opsForValue().get("hk2"));

        //设置对象
       // ValueOperations<String, City> operations = redisTemplate.opsForValue();
        //operations.set("liyu", city);
        // 缓存存在
//        boolean hasKey = redisTemplate.hasKey("liyu");
//        if (hasKey) {
//            System.out.println("....>>"+operations.get("liyu"));
//        }
        return cityDao.findByName(cityName);
    }
}
