package org.spring.springcloud.service.impl;

import org.spring.springcloud.dao.MenuDao;
import org.spring.springcloud.domain.Menu;
import org.spring.springcloud.domain.Role;
import org.spring.springcloud.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {


    @Autowired
    MenuDao menuDao;
    @Override
    public List<Role> getAllRoleSByUrl(String url) {
        return null;
    }

    public List<Menu> getMenusByHrId() {
        return null;
    }

    @Override
    public List<Menu> menuTree() {
        return null;
    }

    @Override
    public List<Long> getMenusByRid(Long rid) {
        return null;
    }
}
