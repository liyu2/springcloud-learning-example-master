package org.spring.springcloud.service;

import org.spring.springcloud.domain.UserRole;

import java.util.List;

public interface UserRoleService {
    public List<UserRole> findRoleByUserId(String UserId);
}
