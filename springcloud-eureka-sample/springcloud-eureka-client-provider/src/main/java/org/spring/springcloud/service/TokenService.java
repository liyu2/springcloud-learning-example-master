package org.spring.springcloud.service;


import org.spring.springcloud.domain.Result;
import org.spring.springcloud.domain.Token;

import java.text.ParseException;


/**
 * @title 页面功能模块
 * @author liyu
 * @version 1
 */
public interface TokenService  {
	/**
	 * 通过用户id获得token
	 * 
	 * @param userId
	 * @return token
	 */
	public Result checkToken(String userId,String token) throws ParseException;

	public Token getTokenByUserId(String userId);


}
