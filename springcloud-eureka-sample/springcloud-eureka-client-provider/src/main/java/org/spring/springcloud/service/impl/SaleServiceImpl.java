package org.spring.springcloud.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.spring.springcloud.dao.CustomDao;
import org.spring.springcloud.dao.SaleDDao;
import org.spring.springcloud.dao.SaleMDao;
import org.spring.springcloud.domain.Custom;
import org.spring.springcloud.domain.SaleD;
import org.spring.springcloud.domain.SaleM;
import org.spring.springcloud.service.SaleService;
import org.spring.springcloud.util.datetime.MyDateUtil;
import org.spring.springcloud.util.nbr.NbrUtils;
import org.spring.springcloud.util.rest.Pagination;
import org.spring.springcloud.util.uuid.UuidUtilsHK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaleServiceImpl implements SaleService {

    @Autowired
    SaleMDao saleMDao;
    @Autowired
    SaleDDao saleDDao;

    @Autowired
    CustomDao customDao;


    @Override
    public SaleM selectSaleMById(String mid) {
        return saleMDao.selectSaleMById(mid);
    }

    @Override
    public Pagination<SaleM> selectMAll(int pageNun,int pageSize) {
        Page page= PageHelper.startPage(pageNun,pageSize);
        List<SaleM> salms= saleMDao.selectMAll();
        Pagination<SaleM> pageInfo = new Pagination<SaleM>(salms, page.getPageNum(), page.getPageSize(), page.getTotal());
        return pageInfo;

    }

    @Override
    @Transactional(readOnly = false)
    public int addSaleM(SaleM salem) {
        salem.setSaleMId(UuidUtilsHK.uuid());
        salem.setNbr(NbrUtils.getOrderIdByUUId());
        salem.setAddTime(MyDateUtil.getDateTime());
        System.out.println("时间="+salem.getAddTime()+"...nbr="+salem.getNbr()+"....id="+salem.getSaleMId());
        return saleMDao.addSaleM(salem);
    }


    @Override
    @Transactional(readOnly = false)
    public int updateSaleM(SaleM salem) {
        return saleMDao.updateSaleM(salem);
    }

    //---------------------------------------------明细操作--------------//
    @Override
    public List<SaleD> selectSaleDByMid(String mid) {
        return saleDDao.selectSaleDByMid(mid);
    }

    @Override
    public SaleD selectSaleDById(String did) {
        return saleDDao.selectSaleDById(did);
    }

    @Override
    public  Pagination<SaleD> selectDAll(int pageNun,int pageSize) {
        Page page= PageHelper.startPage(pageNun,pageSize);
        List<SaleD> salds= saleDDao.selectDAll();
        Pagination<SaleD> pageInfo = new Pagination<SaleD>(salds, page.getPageNum(), page.getPageSize(), page.getTotal());
        return pageInfo;
    }

    @Override
    @Transactional(readOnly = false)
    public int addSaledD(SaleD saled) {
        saled.setSaleDId(UuidUtilsHK.uuid());
        saled.setAddTime(MyDateUtil.getDateTime());
        return saleDDao.addSaledD(saled);
    }

    @Override
    @Transactional(readOnly = false)
    public int updateSaleD(SaleD saled) {
        return saleDDao.updateSaleD(saled);
    }
}
