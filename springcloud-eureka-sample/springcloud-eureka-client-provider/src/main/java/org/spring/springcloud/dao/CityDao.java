package org.spring.springcloud.dao;

import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spring.springcloud.controller.EsCityController;
import org.spring.springcloud.domain.City;


/**
 * ���� DAO �ӿ���
 *
 * Created by bysocket on 07/02/2017.
 */
public interface CityDao {

    /**
     * ���ݳ������ƣ���ѯ������Ϣ
     *
     * @param cityName ������
     */
    City findByName(@Param("cityName") String cityName);
}
