package org.spring.springcloud.config.springSecurity.validate;

import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.web.context.request.ServletWebRequest;

import java.io.IOException;

@RestController
public class ValidateCodeController {


    private  static final  String SESSION_KEY="SESSION_KEY_IMAGE_CODE";
    private SessionStrategy sessionStrategy =new HttpSessionSessionStrategy();

    @GetMapping("/code/image")
    public void createcode(HttpServletRequest request, HttpServletResponse response){

        ImageCode imageCode = createImageCode(request);
        sessionStrategy.setAttribute(new ServletWebRequest(request),SESSION_KEY,imageCode);
        try {
            ImageIO.write(imageCode.getImage(),"JPEG",response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private ImageCode createImageCode(HttpServletRequest request) {

    }
}
