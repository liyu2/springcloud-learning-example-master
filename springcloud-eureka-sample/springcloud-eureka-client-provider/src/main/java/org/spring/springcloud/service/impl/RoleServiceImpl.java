package org.spring.springcloud.service.impl;

import org.spring.springcloud.dao.RoleDao;
import org.spring.springcloud.domain.Role;
import org.spring.springcloud.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleDao roleDao;

    @Override
    public List<Role> getAllRoleSByUrl(String url) {
        return roleDao.getAllRoleSByUrl(url);
    }
}
