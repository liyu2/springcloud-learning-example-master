package org.spring.springcloud.service;

import org.spring.springcloud.domain.Custom;
import org.spring.springcloud.domain.Spl;
import org.spring.springcloud.util.rest.Pagination;

import java.util.List;

public interface CustomService {

    int addCustom(Custom custom);

    int updateCustom(Custom custom);

    Custom selectCustomById(String id);

    Pagination<Custom> selectAll(int pageNun, int pageSize);
}
