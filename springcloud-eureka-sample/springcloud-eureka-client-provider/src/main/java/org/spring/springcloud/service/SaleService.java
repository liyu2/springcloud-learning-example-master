package org.spring.springcloud.service;

import org.spring.springcloud.domain.Custom;
import org.spring.springcloud.domain.SaleD;
import org.spring.springcloud.domain.SaleM;
import org.spring.springcloud.util.rest.Pagination;

import java.util.List;

public interface SaleService {
    SaleM selectSaleMById(String md);

    Pagination<SaleM>  selectMAll(int pageNum, int pageSize);

    int addSaleM(SaleM salem);

    int updateSaleM(SaleM salem);

    List<SaleD> selectSaleDByMid(String mid);

    SaleD selectSaleDById(String id);

    Pagination<SaleD>  selectDAll(int pageNum,int pageSize);

    int addSaledD(SaleD saled);

    int updateSaleD(SaleD saled);


}
