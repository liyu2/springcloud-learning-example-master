package org.spring.springcloud.controller;

import org.spring.springcloud.domain.SaleD;
import org.spring.springcloud.domain.SaleM;
import org.spring.springcloud.service.SaleService;
import org.spring.springcloud.util.rest.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sale")
public class SaleController {

    @Autowired
    SaleService saleservice;


    /**
     * 根据id 查询销售订单
     * @param mid
     * @return
     */
    @RequestMapping(value = "/m/selectSaleMById",method = RequestMethod.GET)
    public Result selectSaleMById(@RequestParam(value = "mid") String mid) {
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.selectSaleMById(mid));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
        }
        return result;
    }

    /**
     * 查询所有销售订单
     * @return
     */
    @RequestMapping(value = "/m/selectMAll",method = RequestMethod.GET)
    public Result selectMAll(@RequestParam(defaultValue = "1") int pageNum
            ,@RequestParam(defaultValue = "10") int pageSize) {
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.selectMAll(pageNum,pageSize));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
        }
        return result;

    }

    /**
     * 添加销售订单
     * @param sale
     * @return
     */
    @RequestMapping(value = "/m/addSaleM",method = RequestMethod.POST)
    public Result addSale(SaleM sale) {
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.addSaleM(sale));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }

    /**
     * 更新销售订单
     * @param sale
     * @return
     */
    @RequestMapping(value = "/m/updateSaleM",method = RequestMethod.POST)
    public Result updateSale(SaleM sale) {
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.updateSaleM(sale));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }

    /**
     * 查询销售该订单所有明细
     * @param mid
     * @return
     */
    @RequestMapping(value = "d/selectSaleDByMid",method = RequestMethod.GET)
    public  Result selectSaleDByMid(String mid){
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.selectSaleDByMid(mid));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }


    /**
     * 根据did查询销售明细表
     * @param did
     * @return
     */
    @RequestMapping(value = "d/selectSaleDById",method = RequestMethod.GET)
    public  Result selectSaleDById(String did){
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.selectSaleDById(did));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }


    /**
     * 查询所有销售明细表
     * @return
     */
    @RequestMapping(value = "d/selectDAll",method = RequestMethod.GET)
    public  Result selectDAll(@RequestParam(defaultValue = "1") int pageNum
            ,@RequestParam(defaultValue = "10") int pageSize){
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.selectDAll(pageNum,pageSize));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }

    /**
     * 添加明细
     * @param sale
     * @return
     */
    @RequestMapping(value = "d/addSaledD",method = RequestMethod.POST)
    public  Result addSaledD(SaleD sale){
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.addSaledD(sale));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }

    @RequestMapping(value = "d/updateSaleD",method = RequestMethod.GET)
    public  Result updateSaleD(SaleD sale){
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(saleservice.updateSaleD(sale));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }
}
