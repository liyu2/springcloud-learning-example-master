package org.spring.springcloud.controller;



import org.spring.springcloud.domain.Custom;
import org.spring.springcloud.domain.SaleM;
import org.spring.springcloud.service.CustomService;
import org.spring.springcloud.util.rest.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/custom")
public class CustomController {
   @Autowired
    CustomService customService;


    @RequestMapping(value = "/selectCustomById",method = RequestMethod.GET)
    public Result selectCustomById(@RequestParam(value = "id") String id) {
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(customService.selectCustomById(id));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
        }
        return result;
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/selectAllCustom",method = RequestMethod.GET)
    public Result selectAllCustom(@RequestParam(defaultValue = "1") int pageNum
            ,@RequestParam(defaultValue = "10") int pageSize) {
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(customService.selectAll(pageNum,pageSize));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
        }
        return result;

    }


    @RequestMapping(value = "/addCustom",method = RequestMethod.POST)
    public Result addCustom(Custom custom) {
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(customService.addCustom(custom));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }


    @RequestMapping(value = "/updateSaleM",method = RequestMethod.POST)
    public Result updateCustom(Custom custom) {
        Result result =new Result();
        result.setRetCode(Result.RECODE_SUCCESS);
        try {
            result.setData(customService.updateCustom(custom));
        }catch (Exception e){
            result.setRetCode(Result.RECODE_ERROR);
            result.setErrMsg(e.toString());
        }
        return result;
    }

}
