//package org.spring.springcloud.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.DirectExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitAdmin;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.beans.factory.config.ConfigurableBeanFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//
///**
// * Created by liyu2 on 2017/9/29
// * 配置消息队列的基本信息
// */
//@Configuration
//@Slf4j
//public class AmqpConfig {
//    /**
//     * FOO_EXCHANGE:路由中心关键字
//     * FOO_ROUTINGKEY：队列绑定key
//     * FOO_QUEUE:消息队列名
//     */
//    public static final String FRIEND_EXCHANGE = "callback.exchange.addfriend";
//    public static final String FRIEND_ROUTINGKEY = "callback.routingkey.addfriend";
//    public static final String FRIEND_QUEUE = "callback.queue.addfriend";
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(AmqpConfig.class);
//
//    @Value("${spring.rabbitmq.addresses}")
//    private String addresses;
//    @Value("${spring.rabbitmq.username}")
//    private String username;
//    @Value("${spring.rabbitmq.password}")
//    private String password;
//    @Value("${spring.rabbitmq.virtual-host}")
//    private String virtualHost;
//    @Value("${spring.rabbitmq.publisher-confirms}")
//    private boolean publisherConfirms;
//
//    /**
//     * 连接rabbitmq
//     *
//     * @return
//     */
//    @Bean
//    public ConnectionFactory connectionFactory() {
//        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
//        //设置MabbitMQ所在主机ip或者主机名
//        connectionFactory.setAddresses(addresses);
//        connectionFactory.setUsername(username);
//        connectionFactory.setPassword(password);
//        connectionFactory.setVirtualHost(virtualHost);
//        /** 如果要进行消息回调，则这里必须要设置为true */
//        connectionFactory.setPublisherConfirms(publisherConfirms);
//        return connectionFactory;
//    }
//
//    /**
//     * rabbitAdmin代理类
//     *
//     * @return
//     */
//    @Bean
//    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
//        return new RabbitAdmin(connectionFactory);
//    }
//
//    /**
//     * 创建rabbitTemplate 消息模板类
//     * prototype原型模式:每次获取Bean的时候会有一个新的实例
//     * 因为要设置回调类，所以应是prototype类型，如果是singleton类型，则回调类为最后一次设置
//     *
//     * @return
//     */
//    @Bean
//    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//    public RabbitTemplate rabbitTemplate() {
//
//        RabbitTemplate template = new RabbitTemplate(connectionFactory());
//        // 设置message序列化方法
//        template.setMessageConverter(new Jackson2JsonMessageConverter());
//
//        return template;
//    }
//
//
//    /**
//     * 针对消费者配置
//     * 1. 设置交换机类型
//     * 2. 将队列绑定到交换机
//     * <p>
//     * FanoutExchange: 将消息分发到所有的绑定队列，无routingkey的概念
//     * HeadersExchange ：通过添加属性key-value匹配
//     * DirectExchange:按照routingkey分发到指定队列
//     * TopicExchange:多关键字匹配
//     */
//    @Bean
//    public DirectExchange defaultExchange() {
//        return new DirectExchange(FRIEND_EXCHANGE);
//    }
//    @Bean
//    public Queue queue() {
//        return new Queue(FRIEND_QUEUE, true); //队列持久
//    }
//    @Bean
//    public Binding binding() {
//        return BindingBuilder.bind(queue()).to(defaultExchange()).with(AmqpConfig.FRIEND_ROUTINGKEY);
//    }
//
//
//}
