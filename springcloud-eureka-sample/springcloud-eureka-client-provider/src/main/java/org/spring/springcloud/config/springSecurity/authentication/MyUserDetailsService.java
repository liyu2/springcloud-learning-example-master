package org.spring.springcloud.config.springSecurity.authentication;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spring.springcloud.domain.Role;
import org.spring.springcloud.domain.UserEntity;
import org.spring.springcloud.service.UserRoleService;
import org.spring.springcloud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
/**
 * 功能：根据前端页面传入的username来查询数据的用户以及角色
 */
@Component
public class MyUserDetailsService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsService.class);
    @Autowired
    UserService userService;

    @Autowired
    UserRoleService userRoleService;
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        logger.info("MyUserDetailsService");
        //数据库连的user
        UserEntity userEntity;
        try {
            userEntity = userService.findUserByUserName(userName);
            logger.info("服务器用户名为="+userEntity.getUserName());
        } catch (Exception e) {
            throw new UsernameNotFoundException("查询报错");
        }
        if(userEntity == null)
        {
            throw new UsernameNotFoundException("用户不存在");
        }
        else
        {
            try {
                List<Role> roles=userEntity.getRoles();
                List<SimpleGrantedAuthority> simpleGrantedAuthorities = createAuthorities(roles);
                return new User(userEntity.getUserName(), userEntity.getPassword(), simpleGrantedAuthorities);
            } catch (Exception e) {
                throw new UsernameNotFoundException("查询用户权限报错");
            }

        }
    }
    /**
     * 权限字符串转化
     *
     * 如 "USER,ADMIN" -> SimpleGrantedAuthority("USER") + SimpleGrantedAuthority("ADMIN")
     */
    private List<SimpleGrantedAuthority> createAuthorities(List<Role> roles){
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        for (Role role : roles) {
            simpleGrantedAuthorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        }
        return simpleGrantedAuthorities;
    }
}
