package org.spring.springcloud.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.spring.springcloud.domain.Role;

import java.util.List;


public interface MenuDao {
    public List<Role> getAllRoleSByUrl(String url);
}
