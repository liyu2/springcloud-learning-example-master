package org.spring.springcloud.dao;

import org.spring.springcloud.domain.Role;

import java.util.List;

public interface RoleDao {
    public List<Role> getAllRoleSByUrl(String url);
}
