package org.spring.springcloud.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.spring.springcloud.dao.CustomDao;
import org.spring.springcloud.domain.Custom;
import org.spring.springcloud.domain.Spl;
import org.spring.springcloud.service.CustomService;
import org.spring.springcloud.util.datetime.MyDateUtil;
import org.spring.springcloud.util.rest.Pagination;
import org.spring.springcloud.util.uuid.UuidUtilsHK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomServiceImpl implements CustomService {
    @Autowired
    CustomDao customDao;
    @Override
    public int addCustom(Custom custom) {
        custom.setCustomId(UuidUtilsHK.uuid());
        custom.setAddTime(MyDateUtil.getDateTime());
        return customDao.addCustom(custom);
    }

    @Override
    public int updateCustom(Custom custom) {
        custom.setModTime(MyDateUtil.getDateTime());
        return customDao.updateCustom(custom);
    }

    @Override
    public Custom selectCustomById(String id) {
        return customDao.selectCustomById(id);
    }

    @Override
    public Pagination<Custom> selectAll(int pageNun, int pageSize) {
        Page page= PageHelper.startPage(pageNun,pageSize);
        List<Custom> customs= customDao.selectAll();
        Pagination<Custom> pageInfo = new Pagination<Custom>(customs, page.getPageNum(), page.getPageSize(), page.getTotal());
        return pageInfo;
    }
}
