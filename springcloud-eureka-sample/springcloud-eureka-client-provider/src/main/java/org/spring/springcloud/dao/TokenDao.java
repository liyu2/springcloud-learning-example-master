package org.spring.springcloud.dao;

import org.apache.ibatis.annotations.Param;
import org.spring.springcloud.domain.Token;

/**
 * @author liyu
 */
public interface TokenDao {

	public Token getTokenByUserId(@Param("userId") String userId);

}
