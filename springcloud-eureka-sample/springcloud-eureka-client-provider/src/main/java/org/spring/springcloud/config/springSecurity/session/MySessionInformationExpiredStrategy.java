package org.spring.springcloud.config.springSecurity.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Created by liyu on 2018/5/12.
 * 功能：登录被踢掉时的自定义操作
 */
@Component
public class MySessionInformationExpiredStrategy implements SessionInformationExpiredStrategy {
    private static final Logger logger = LoggerFactory.getLogger(MySessionInformationExpiredStrategy.class);
    @Override
    public void onExpiredSessionDetected(SessionInformationExpiredEvent sessionInformationExpiredEvent) throws IOException, ServletException {
        logger.info("并发登陆，不好意思，你被踢出登陆了");
        System.out.println("并发登陆，不好意思，你被踢出登陆了");
        sessionInformationExpiredEvent.getResponse().getWriter().write("并发登陆，不好意思，你被踢出登陆了");
    }
}
