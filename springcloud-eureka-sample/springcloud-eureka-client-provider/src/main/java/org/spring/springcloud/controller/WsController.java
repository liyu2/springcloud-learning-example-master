package org.spring.springcloud.controller;

import org.spring.springcloud.domain.RequestMessage;
import org.spring.springcloud.domain.ResponseMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Created by liyu on 2017/11/6.
 */
@Controller
public class WsController {

    /**
     * message”/topic/getResponse”这个订阅路径中,只要客户端订阅了这条路径，不管是哪个用户，都会接收到消息
     * @param message
     * @return
     */
    @MessageMapping("/welcome")  //@MessageMapping接收客户端消息
    @SendTo("/topic/getResponse") //@SendTo广播消息出去
    public ResponseMessage say(RequestMessage message) {
        System.out.println(message.getName());
        return new ResponseMessage("welcome," + message.getName() + " !");
    }
}
