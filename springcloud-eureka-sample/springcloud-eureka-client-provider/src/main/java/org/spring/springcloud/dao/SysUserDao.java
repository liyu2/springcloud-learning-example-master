package org.spring.springcloud.dao;

import org.spring.springcloud.domain.SysUser;

public interface SysUserDao {

    SysUser findUserByUserName(String userName);
}
