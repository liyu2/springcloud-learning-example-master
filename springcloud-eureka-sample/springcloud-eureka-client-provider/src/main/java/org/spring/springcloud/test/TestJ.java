//package org.spring.springcloud.test;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.spring.springcloud.ProviderApplication;
//import org.spring.springcloud.domain.User;
//import org.spring.springcloud.domain.UserEntity;
//import org.spring.springcloud.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//
//@RunWith(SpringJUnit4ClassRunner.class) // SpringJUnit支持，由此引入Spring-Test框架支持！
//@SpringApplicationConfiguration(classes = ProviderApplication.class) // 指定我们SpringBoot工程的Application启动类
//@WebAppConfiguration // 由于是Web项目，Junit需要模拟ServletContext，因此我们需要给我们的测试类加上@WebAppConfiguration。
//
//
//public class TestJ {
//
//    @Autowired
//    UserService userService;
//
//    @Test
//    public void testShow() {
//      UserEntity user=  userService.findUserByUserName("liyu");
//        System.out.println(user.getRoles().get(0).getRoleName());
//    }
//}
