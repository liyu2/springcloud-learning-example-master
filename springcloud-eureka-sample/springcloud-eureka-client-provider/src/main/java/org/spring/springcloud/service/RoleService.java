package org.spring.springcloud.service;

import org.spring.springcloud.domain.Role;

import java.util.List;

public interface RoleService {
    public List<Role> getAllRoleSByUrl(String url);
}
